// Copyright (C) 2022, California Institute of Technology and contributors
//
// This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
// If a copy of the MPL was not distributed with this file, You can obtain one at
// <https://mozilla.org/MPL/2.0/>.
//
// SPDX-License-Identifier: MPL-2.0

use std::collections::HashMap;
use std::env;
use std::error::Error;
use std::pin::Pin;
use std::sync::Arc;

use arrow::{
    array::{as_string_array, Int64Array},
    datatypes::{Schema, SchemaRef},
    ipc::{self, reader},
    record_batch::RecordBatch,
};
use arrow_flight::flight_service_client::FlightServiceClient;
use arrow_flight::{utils::flight_data_to_arrow_batch, FlightData, FlightDescriptor};
use futures::stream::{Stream, StreamExt};
use serde::{Deserialize, Serialize};
use tonic::transport::Channel;
use tonic::{Status, Streaming};

pub static DEFAULT_ARRAKIS_SERVER: &str = "http://localhost:31206";

#[derive(Serialize, Deserialize, Debug)]
struct StreamRequest {
    channels: Vec<String>,
    start: Option<i32>,
    end: Option<i32>,
}

#[derive(Serialize, Deserialize, Debug)]
struct DescribeRequest {
    channels: Vec<String>,
}

#[derive(Serialize, Deserialize, Debug)]
struct MatchRequest {
    pattern: Option<String>,
    data_type: Option<String>,
    min_rate: Option<i32>,
    max_rate: Option<i32>,
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(tag = "request", content = "args")]
enum Request {
    Stream(StreamRequest),
    Describe(DescribeRequest),
    Find(MatchRequest),
    Count(MatchRequest),
}

/// Create a flight descriptor from a Request-like command
fn create_flight_descriptor(request: Request) -> serde_json::Result<FlightDescriptor> {
    // serialize request
    let cmd = serde_json::to_string(&request)?;

    // create a descriptor from cmd
    Ok(FlightDescriptor::new_cmd(cmd.into_bytes().to_vec()))
}

/// Send a request to an Arrow Flight compatible server
async fn send_flight_request(
    mut client: FlightServiceClient<Channel>,
    request: Request,
) -> Result<Streaming<FlightData>, Box<dyn Error>> {
    let descriptor = create_flight_descriptor(request).expect("Failed to create descriptor");
    let flight = client.get_flight_info(descriptor);

    // extract flight ticket
    let endpoints = flight.await?.into_inner().endpoint;
    let ticket = endpoints[0].ticket.clone().unwrap();

    // send request to server, get response
    let response = client.do_get(ticket).await?.into_inner();
    Ok(response)
}

/// Extract a flight schema from a response
async fn receive_flight_schema(response: &mut Streaming<FlightData>) -> Option<Arc<Schema>> {
    let data = response.message().await.ok()??;
    let message =
        arrow::ipc::root_as_message(&data.data_header[..]).expect("Error parsing message");

    // message header is a Schema, so read it
    let ipc_schema: ipc::Schema = message
        .header_as_schema()
        .expect("Unable to read IPC message as schema");
    let schema = ipc::convert::fb_to_schema(ipc_schema);

    Some(Arc::new(schema))
}

/// Maps a chunked iterator into a record batch
async fn chunked_flight_data_to_batch(
    chunk: Vec<Result<FlightData, Status>>,
    schema: SchemaRef,
) -> Option<RecordBatch> {
    let mut dictionaries_by_id = HashMap::new();
    let mut data = chunk[0].as_ref().ok()?;
    let version = ipc::MetadataVersion::V5;
    let mut message =
        arrow::ipc::root_as_message(&data.data_header[..]).expect("Error parsing first message");

    while message.header_type() == ipc::MessageHeader::DictionaryBatch {
        reader::read_dictionary(
            &data.data_body,
            message
                .header_as_dictionary_batch()
                .expect("Error parsing dictionary"),
            &schema,
            &mut dictionaries_by_id,
            &version,
        )
        .expect("Error reading dictionary");

        data = chunk[1].as_ref().ok()?;
        message =
            arrow::ipc::root_as_message(&data.data_header[..]).expect("Error parsing message");
    }
    let batch = flight_data_to_arrow_batch(data, schema.clone(), &dictionaries_by_id)
        .expect("Unable to convert flight data to Arrow batch");

    Some(batch)
}

/// Collect all record batches in a stream into a single record batch
async fn collect_into_record_batch(
    mut stream: Pin<Box<impl Stream<Item = Option<RecordBatch>>>>,
    schema: &Arc<Schema>,
) -> Result<RecordBatch, Box<dyn Error>> {
    let mut blocks = Vec::new();
    while let Some(block) = stream.next().await {
        blocks.push(block.unwrap())
    }

    // convert record batch into count
    Ok(RecordBatch::concat(schema, &blocks[..])?)
}

/// Map a flight data stream into a stream of record batches
async fn generate_flight_stream(
    response: Streaming<FlightData>,
    schema: Arc<Schema>,
) -> Result<Pin<Box<impl Stream<Item = Option<RecordBatch>>>>, Box<dyn Error>> {
    let stream = response
        .chunks(2)
        .then(move |x| chunked_flight_data_to_batch(x, schema.clone()));

    Ok(Box::pin(stream))
}

/// Connect to an Arrow Flight server
///
/// If the URL is not set, it will first check whether ARRAKIS_SERVER is set,
/// otherwise use the default server.
///
async fn connect(
    maybe_url: Option<String>,
) -> Result<FlightServiceClient<Channel>, tonic::transport::Error> {
    let url = match maybe_url {
        Some(x) => x,
        None => env::var("ARRAKIS_SERVER").unwrap_or_else(|_| DEFAULT_ARRAKIS_SERVER.to_string()),
    };
    FlightServiceClient::connect(url).await
}

/// Stream live or offline timeseries data
///
/// # Arguments
///
/// * `channels` - A list of channels to request
/// * `start` - The start time, if specified
/// * `end` - The end time, if specified
///
/// Setting neither `start` nor `end` starts a live stream
/// from now.
///
pub async fn stream(
    channels: Vec<String>,
    start: Option<i32>,
    end: Option<i32>,
) -> Result<impl Stream<Item = Option<RecordBatch>>, Box<dyn Error>> {
    // connect to client
    let client = connect(None).await?;

    // create and send request
    let request = Request::Stream(StreamRequest {
        channels,
        start,
        end,
    });
    let mut response = send_flight_request(client, request).await?;

    // extract schema
    let schema = receive_flight_schema(&mut response)
        .await
        .expect("Failed to receive flight schema");

    // map response to record batches
    generate_flight_stream(response, schema.clone()).await
}

/// Get channel metadata for channels requested
///
/// # Arguments
///
/// * `channels` - A list of channels to request
///
pub async fn describe(
    channels: Vec<String>,
) -> Result<Vec<String>, Box<dyn Error>> {
    // connect to client
    let client = connect(None).await?;

    // create and send request
    let request = Request::Describe(DescribeRequest {
        channels
    });
    let mut response = send_flight_request(client, request).await?;

    // extract schema
    let schema = receive_flight_schema(&mut response)
        .await
        .expect("Failed to receive flight schema");

    // collect response into record batch
    let stream = generate_flight_stream(response, schema.clone()).await?;
    let batch = collect_into_record_batch(stream, &schema).await?;

    // convert record batch into channel list
    let array = as_string_array(batch.column(0));
    let channels = array
        .iter()
        .map(|s| s.unwrap().into())
        .collect::<Vec<String>>();
    Ok(channels)
}


/// Find channels matching a set of conditions
///
/// # Arguments
///
/// * `pattern` - A channel pattern to match channels with
/// * `data_type` - Data types to match, if specified
/// * `min_rate` - Minimum sampling rate for channels, if specified
/// * `min_rate` - Maximum sampling rate for channels, if specified
///
pub async fn find(
    pattern: Option<String>,
    data_type: Option<String>,
    min_rate: Option<i32>,
    max_rate: Option<i32>,
) -> Result<Vec<String>, Box<dyn Error>> {
    // connect to client
    let client = connect(None).await?;

    // create and send request
    let request = Request::Find(MatchRequest {
        pattern,
        data_type,
        min_rate,
        max_rate,
    });
    let mut response = send_flight_request(client, request).await?;

    // extract schema
    let schema = receive_flight_schema(&mut response)
        .await
        .expect("Failed to receive flight schema");

    // collect response into record batch
    let stream = generate_flight_stream(response, schema.clone()).await?;
    let batch = collect_into_record_batch(stream, &schema).await?;

    // convert record batch into channel list
    let array = as_string_array(batch.column(0));
    let channels = array
        .iter()
        .map(|s| s.unwrap().into())
        .collect::<Vec<String>>();
    Ok(channels)
}

/// Count channels matching a set of conditions
///
/// # Arguments
///
/// * `pattern` - A channel pattern to match channels with
/// * `data_type` - Data types to match, if specified
/// * `min_rate` - Minimum sampling rate for channels, if specified
/// * `min_rate` - Maximum sampling rate for channels, if specified
///
pub async fn count(
    pattern: Option<String>,
    data_type: Option<String>,
    min_rate: Option<i32>,
    max_rate: Option<i32>,
) -> Result<i64, Box<dyn Error>> {
    // connect to client
    let client = connect(None).await?;

    // create and send request
    let request = Request::Count(MatchRequest {
        pattern,
        data_type,
        min_rate,
        max_rate,
    });
    let mut response = send_flight_request(client, request).await?;

    // extract schema
    let schema = receive_flight_schema(&mut response)
        .await
        .expect("Failed to receive flight schema");

    // collect response into record batch
    let stream = generate_flight_stream(response, schema.clone()).await?;
    let batch = collect_into_record_batch(stream, &schema).await?;

    // convert record batch into count
    let array = batch
        .column(0)
        .as_any()
        .downcast_ref::<Int64Array>()
        .unwrap();
    Ok(array.value(0))
}
