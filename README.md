<h1 align="center">arrakis-rs</h1>

<p align="center">Arrakis Rust client library</p>

---

## Installation

``` toml
[dependencies]
arrakis = { git = "https://git.ligo.org/ngdd/arrakis-rs" }
```

## Features

* Query live and historical timeseries data
* Describe channel metadata
* Search for channels matching a set of conditions
* Publish timeseries data

## Quickstart

Note: This crate uses Tokio for its asynchronous runtime and therefore, all calls
need to run in Tokio's event loop. Some example usage is shown below.

### Stream timeseries

##### 1. Live data

``` rust
use std::error::Error;

#[tokio::main]
pub async fn main() -> Result<(), Box<dyn Error>> {
    let channels = vec![String::from("H1:CAL-DELTAL_EXTERNAL_DQ")];

    // retrieve live streaming data
    let mut stream = arrakis::stream(channels, None, None).await?;

    // process blocks as they become available
    while let Some(block) = stream.next().await {
        println!("{:?}", block);
    }
```

##### 2. Historical data

``` rust
use std::error::Error;

#[tokio::main]
pub async fn main() -> Result<(), Box<dyn Error>> {
    let channels = vec![String::from("H1:CAL-DELTAL_EXTERNAL_DQ")];
    let start = Some(1234500000);
    let end = Some(1234500100);

    // retrieve 100s of streaming data
    let mut stream = arrakis::stream(channels, start, end).await?;

    // process blocks as they become available
    while let Some(block) = stream.next().await {
        println!("{:?}", block);
    }
```

### Fetch timeseries

TODO

### Describe metadata

``` rust
use std::error::Error;

#[tokio::main]
pub async fn main() -> Result<(), Box<dyn Error>> {
    let channels = vec![String::from("H1:CAL-DELTAL_EXTERNAL_DQ")];

    // get metadata for channels requested
    let metadata = arrakis::describe(channels).await?;
    println!("metadata: {:?}", metadata);
```

### Find channels

``` rust
use std::error::Error;

#[tokio::main]
pub async fn main() -> Result<(), Box<dyn Error>> {
    // channel pattern to match
    let pattern = Some(String::from("H1:CAL-*"));

    // find channels
    let channel_list = arrakis::find(pattern, None, None, None).await?;
    println!("channels: {:?}", channel_list);
```

### Count channels

``` rust
use std::error::Error;

#[tokio::main]
pub async fn main() -> Result<(), Box<dyn Error>> {
    // channel pattern to match
    let pattern = Some(String::from("H1:CAL-*"));

    // count channels
    let count = arrakis::count(pattern, None, None, None).await?;
    println!("num channels: {:?}", count);
```

### Publish timeseries

TODO
